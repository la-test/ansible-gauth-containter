FROM alpine:3.8 as build
RUN apk add --no-cache \
    python3=3.6.4-r1 \
    python3-dev=3.6.4-r1 \
    musl-dev \
    libffi-dev \
    openssl-dev \
    make \
    gcc && \
    pip3 install --upgrade pip setuptools wheel
RUN mkdir /tmp/build
COPY requirements.txt /tmp/build/
RUN cat /tmp/build/requirements.txt | xargs pip3 wheel --wheel-dir=/tmp/build/wheels

FROM alpine:3.8
COPY --from=build /tmp/build /tmp/build
RUN apk add --no-cache python3=3.6.4-r1 && \
    pip3 install --upgrade pip --no-index \
        --find-links=/tmp/build/wheels -r /tmp/build/requirements.txt && \
    rm -rf /root/.cache /tmp/build
CMD ["/sbin/init"]
